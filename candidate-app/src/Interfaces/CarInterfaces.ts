export default interface ICar {
  id: Number;
  name: string;
  plateNumber: string;
  brand: string;
  gen: string;
  remark: string;
}
