export default interface ICarColumn {
  key?: string;
  title: string;
  render?: any;
  dataIndex?: string;
}
