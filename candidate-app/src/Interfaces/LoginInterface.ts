import React from "react";

export interface ILogin {
  username: string;
  password: string;
}

export interface ILoginProps {
  setCredential: React.Dispatch<React.SetStateAction<string>>;
  credential: string;
}
