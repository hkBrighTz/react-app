import { AddPage, MainPage, EditPage, LoginPage } from "../components";
import IRoute from "../Interfaces/RouteInterface";

const routes: IRoute[] = [
  {
    path: "/",
    name: "Home Page",
    exact: true,
    component: MainPage,
  },
  {
    path: "/addPage",
    name: "Add Page",
    exact: true,
    component: AddPage,
  },
  {
    path: "/editPage/:id",
    name: "Edit Page",
    exact: true,
    component: EditPage,
  },
  {
    path: "/LoginPage",
    name: "Login Page",
    exact: false,
    component: LoginPage,
  },
];

export default routes;
