import { Table } from "antd";
import React from "react";

interface Input {
  renderData: any;
  renderColumns: any;
}

const TableCustom: React.FC<Input> = ({ renderData, renderColumns }) => {
  return (
    <div>
      <Table columns={renderColumns} dataSource={renderData} />
    </div>
  );
};

export default TableCustom;
