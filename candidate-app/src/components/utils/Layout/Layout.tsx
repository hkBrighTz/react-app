import { Button, Col } from "antd";
import React from "react";
import { Link } from "react-router-dom";
import { includes } from "lodash";
import routes from "../../../route/route";
import Logo from "../../../asset/Haup-Logo-04.webp";
import "./Layout.css";

const Layout: React.FC<any> = ({ children, logout }) => {
  return (
    <div>
      <section className="container">
        <Col span={6} />
        <Col span={12} className="wrap-content">
          <img src={Logo} alt="Logo" />
          <div className="menu-page">
            {routes.map((route) => {
              if (!includes(["Edit Page", "Login Page"], route.name)) {
                return (
                  <Button type="primary">
                    <Link to={route.path}>{route.name}</Link>
                  </Button>
                );
              } else {
                return "";
              }
            })}
          </div>
        </Col>
        <Col className="" span={6}>
          <Button onClick={logout}>Logout</Button>
        </Col>
      </section>
      {children}
    </div>
  );
};

export default Layout;
