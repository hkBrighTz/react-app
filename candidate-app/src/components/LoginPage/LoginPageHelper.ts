import { ILogin, ILoginProps } from "../../Interfaces/LoginInterface";

export const useLogin = ({ setCredential, credential }: ILoginProps) => {
  const login = ({ username, password }: ILogin) => {
    if (!localStorage.getItem("token_login") && !credential) {
      if (username === "admin" && password === "admin") {
        localStorage.setItem("token_login", "ok");
        setCredential("ok");
        window.location.href = "/";
      }
    } else {
      window.location.href = "/";
    }
  };
  return { login };
};
