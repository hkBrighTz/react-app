import React, { useEffect } from "react";
import { Form, Input, Button } from "antd";
import { useLogin } from "./LoginPageHelper";
import "./LoginPage.css";
import { ILoginProps } from "../../Interfaces/LoginInterface";
import Logo from "../../asset/Haup-Logo-04.webp";

const LoginPage: React.FC<ILoginProps> = ({ setCredential, credential }) => {
  const { login } = useLogin({ setCredential, credential });

  useEffect(() => {
    if (credential || localStorage.getItem("token_login")) {
      window.location.href = "/";
    }
  }, [credential]);

  return (
    <div className="login-container">
      <Form
        name="basic"
        initialValues={{ remember: true }}
        onFinish={login}
        autoComplete="off"
      >
        <img className="img-login" src={Logo} alt="Logo" />
        <Form.Item
          label="Username"
          name="username"
          rules={[{ required: true, message: "Please input your username!" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
      <span> username and password is admin</span>
    </div>
  );
};

export default LoginPage;
