import React, { ReactElement, useEffect } from "react";
import { Link } from "react-router-dom";
import { Button, Col, Space } from "antd";
import TableCustom from "../utils/Table/Table";
import "./MainPage.css";
import { useCarhandler } from "./MainPageHelper";
import ICar from "../../Interfaces/CarInterfaces";

const MainPage: React.FC = () => {
  const { getAllCar, allCar, handlerDelete } = useCarhandler();

  useEffect(() => {
    getAllCar();
  }, []);

  const columns = [
    {
      key: "key",
      title: "ชื่อ",
      dataIndex: "name",
    },
    {
      title: "ทะเบียน",
      dataIndex: "plateNumber",
    },
    {
      title: "ยี่ห้อ",
      dataIndex: "brand",
    },
    {
      title: "รุ่น",
      dataIndex: "gen",
    },
    {
      title: "หมายเหตุ",
      dataIndex: "remark",
    },
    {
      title: "ลบ",
      key: "action",
      render: (e: ICar): ReactElement => (
        <Space size="middle">
          <Button
            onClick={() => handlerDelete(e.name, e.plateNumber)}
            type="primary"
          >
            Delete
          </Button>
        </Space>
      ),
    },
    {
      title: "แก้ไข",
      key: "action",
      render: (e: ICar): ReactElement => (
        <Space size="middle">
          <Link to={`./editPage/${e.id}`}>Edit</Link>
        </Space>
      ),
    },
  ];

  return (
    <div className="main-container">
      <Col span={3} />
      <Col span={18}>
        <TableCustom renderColumns={columns} renderData={allCar} />
      </Col>
      <Col span={3} />
    </div>
  );
};

export default MainPage;
