import { filter, find, isEmpty } from "lodash";
import { useState } from "react";
import ICar from "../../Interfaces/CarInterfaces";

export const useCarhandler = () => {
  const [allCar, setAllCar] = useState<ICar[]>([]);

  const getAllCar = () => {
    const allCar = JSON.parse(localStorage.getItem("listCar") || "[]");
    const data = allCar.map((car: ICar) => {
      return {
        id: car.id,
        name: car.name,
        plateNumber: car.plateNumber,
        brand: car.brand,
        gen: car.gen,
        remark: car.remark,
      };
    });
    setAllCar(data);
  };

  const handlerDelete = (name: string, plateNumber: string) => {
    const isFound = !isEmpty(
      find(allCar, {
        name,
        plateNumber,
      })
    );
    if (isFound) {
      const filterOutCar = filter(
        allCar,
        (car: ICar) => car.name !== name && car.plateNumber !== plateNumber
      );
      setAllCar(filterOutCar);
      localStorage.setItem("listCar", JSON.stringify(filterOutCar));
    }
  };

  return {
    getAllCar,
    allCar,
    handlerDelete,
  };
};
