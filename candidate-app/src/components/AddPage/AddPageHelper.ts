import { find } from "lodash";
import ICar from "../../Interfaces/CarInterfaces";

export const useHandlerCar = () => {
  const handlerAddCar = (e: ICar) => {
    const car = localStorage.getItem("listCar");
    const allCar = JSON.parse(car || "[]");
    const wrapData = {
      id: Math.floor(Math.random() * 1000000000),
      name: e.name,
      plateNumber: e.plateNumber,
      brand: e.brand,
      gen: e.gen,
      remark: e.remark,
    };
    const result = find(allCar, {
      plateNumber: wrapData.plateNumber,
    });
    if (!result) {
      allCar.push(wrapData);
      localStorage.setItem("listCar", JSON.stringify(allCar));
      alert("Added Data");
      window.location.reload();
    } else {
      alert("Duplicate data");
    }
  };

  return { handlerAddCar };
};
