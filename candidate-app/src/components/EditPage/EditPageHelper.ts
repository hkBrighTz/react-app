import { isEmpty, find, findIndex } from "lodash";
import { useState } from "react";
import ICar from "../../Interfaces/CarInterfaces";

export const useGetCar = () => {
  const [data, setData] = useState<ICar>();

  const getCarData = async (param: any) => {
    const allCar = JSON.parse((await localStorage.getItem("listCar")) || "[]");
    const isFound = !isEmpty(find(allCar, { id: parseInt(param.id) }));
    if (isFound) {
      const index = findIndex(allCar, { id: parseInt(param.id) });
      setData(allCar[index]);
    } else {
      alert("Not found data");
      window.location.href = "../";
    }
  };

  const editHandler = async (item: ICar, param: any) => {
    const allCar = JSON.parse((await localStorage.getItem("listCar")) || "[]");
    const isFound = !isEmpty(
      find(allCar, {
        id: item.plateNumber,
      })
    );
    if (isFound) {
      alert("Duplicate plate number");
    } else {
      const index = findIndex(allCar, { id: parseInt(param.id) });
      allCar[index] = { ...item, id: parseInt(param.id) };
      await localStorage.setItem("listCar", JSON.stringify(allCar));
      alert("Edited success");
      window.location.reload();
    }
  };

  return {
    getCarData,
    data,
    editHandler,
  };
};
