import React, { useEffect } from "react";
import { Form, Col, Space, Input, Button } from "antd";
import { RouteComponentProps } from "react-router-dom";
import { useGetCar } from "./EditPageHelper";
import { useForm } from "antd/lib/form/Form";
// import "./EditPage.css";

const EditPage: React.FC<RouteComponentProps> = (props) => {
  const [form] = useForm();
  const { getCarData, data, editHandler } = useGetCar();
  form.setFieldsValue({ ...data });

  useEffect(() => {
    getCarData(props.match.params);
  }, []);

  return (
    <div className="main-container">
      <Col span={8} />
      <Col span={8}>
        <Form
          name="insert-form"
          form={form}
          onFinish={(e) => editHandler(e, props.match.params)}
        >
          <div className="wrap-form">
            <Form.Item label="name">
              <Space>
                <Form.Item
                  name="name"
                  noStyle
                  rules={[{ required: true, message: "name is required" }]}
                >
                  <Input
                    style={{ width: 160 }}
                    value={data?.name}
                    placeholder="Please input"
                  />
                </Form.Item>
              </Space>
            </Form.Item>
            <Form.Item label="Plate Number">
              <Space>
                <Form.Item
                  name="plateNumber"
                  noStyle
                  rules={[
                    { required: true, message: "Plate number is required" },
                  ]}
                >
                  <Input
                    style={{ width: 160 }}
                    value={data?.plateNumber}
                    placeholder="Please input"
                  />
                </Form.Item>
              </Space>
            </Form.Item>
            <Form.Item label="Brand">
              <Space>
                <Form.Item
                  name="brand"
                  noStyle
                  rules={[{ required: true, message: "brand is required" }]}
                >
                  <Input
                    style={{ width: 160 }}
                    value={data?.brand}
                    placeholder="Please input"
                  />
                </Form.Item>
              </Space>
            </Form.Item>
            <Form.Item label="Generation">
              <Space>
                <Form.Item
                  name="gen"
                  noStyle
                  rules={[{ required: true, message: "gen is required" }]}
                >
                  <Input
                    style={{ width: 160 }}
                    value={data?.gen}
                    placeholder="Please input"
                  />
                </Form.Item>
              </Space>
            </Form.Item>
            <Form.Item label="Remark">
              <Space>
                <Form.Item
                  name="remark"
                  noStyle
                  rules={[{ required: true, message: "remark is required" }]}
                >
                  <Input style={{ width: 160 }} placeholder="Please input" />
                </Form.Item>
              </Space>
            </Form.Item>
          </div>
          <Form.Item label=" " className="edit-button" colon={false}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Col>
      <Col span={8} />
    </div>
  );
};

export default EditPage;
