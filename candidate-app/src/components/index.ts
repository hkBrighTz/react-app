import MainPage from "./MainPage/MainPage";
import AddPage from "./AddPage/AddPage";
import EditPage from "./EditPage/EditPage";
import LoginPage from "./LoginPage/LoginPage";

export { MainPage, AddPage, EditPage, LoginPage };
