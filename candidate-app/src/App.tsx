import React, { useState } from "react";
import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import routes from "./route/route";
import Layout from "./components/utils/Layout/Layout";
import LoginPage from "./components/LoginPage/LoginPage";
import { ILoginProps } from "./Interfaces/LoginInterface";

const useLogout = ({ setCredential, credential }: ILoginProps) => {
  const logout = () => {
    localStorage.removeItem("token_login");
    setCredential("");
    window.location.href = "";
  };
  return {
    logout,
  };
};

const App: React.FC = () => {
  const [credential, setCredential] = useState<string>("");
  const { logout } = useLogout({ setCredential, credential });

  if (!localStorage.getItem("token_login") && !credential) {
    return <LoginPage setCredential={setCredential} credential={credential} />;
  }

  return (
    <BrowserRouter>
      <Switch>
        <Layout logout={logout}>
          {routes.map((route, index) => {
            return (
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                component={route.component}
              />
            );
          })}
        </Layout>
      </Switch>
    </BrowserRouter>
  );
};

export default App;
